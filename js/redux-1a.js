import React from 'react'
import ReactDOM from 'react-dom'
import _ from 'lodash'
import { Component } from 'react'
import { createStore } from 'redux'
import { Provider, connect } from 'react-redux'


const ADD_TRUFFEL = 'ADD_TRUFFEL'


function reducer (truffels = [], action) {

    switch (action.type) {

        case ADD_TRUFFEL:
            return truffels.concat([action.name])

        default:
            return truffels

    }

}

function addTruffel (name) {
    return {
        type: ADD_TRUFFEL,
        name: name
    }
}

const store = createStore(reducer)

function mapStateToProps (truffels) {
    return {
        truffels: truffels
    }
}

function mapDispatchToProps (dispatch) {
    return {
        onAddTruffelClick: name => {
            dispatch(addTruffel(name))
        }
    }
}


@connect(mapStateToProps, mapDispatchToProps)
class Truffels1a_Redux extends Component {

    render () {
        return (
            <div>
                <button onClick={() => this.props.onAddTruffelClick('truffel')}>add truffel</button>
                {this.getTruffelEls()}
            </div>
        )
    }

    getTruffelEls () {
        return _.map(this.props.truffels, (truffel, index) => {
            return <div key={index}>{truffel}</div>
        })
    }

}


ReactDOM.render(
    <Provider store={store}>
        <Truffels1a_Redux />
    </Provider>,
    document.getElementById('redux-1a')
)
