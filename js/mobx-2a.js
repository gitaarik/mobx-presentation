import React from 'react'
import ReactDOM from 'react-dom'
import _ from 'lodash'
import { Component } from 'react'
import { observable, action, computed } from 'mobx'
import { observer } from 'mobx-react'


class Store {

    @observable truffels = []

    @action addTruffel (name) {
        this.truffels.push(name)
    }

    getTotalTruffels () {
        return this.truffels.length
    }

}


const store = new Store()


@observer
class Truffels extends Component {

    render () {
        return (
            <div>
                <button onClick={() => this.props.onAddTruffelClick('truffels')}>add truffel</button>
                <TruffelCounter getTotalTruffels={this.props.getTotalTruffels} />
                {this.getTruffelEls()}
            </div>
        )
    }

    getTruffelEls () {
        return _.map(this.props.truffels, (truffel, index) => {
            return <div key={index}>{truffel}</div>
        })
    }

}


class TruffelCounter extends Component {

    render () {
        return (
            <div>Total truffels: {this.props.getTotalTruffels()}</div>
        )
    }

}


ReactDOM.render(
    <Truffels
        truffels={store.truffels}
        onAddTruffelClick={name => store.addTruffel(name)}
        getTotalTruffels={() => store.getTotalTruffels()}
    />,
    document.getElementById('mobx-2a')
)
