import React from 'react'
import ReactDOM from 'react-dom'
import _ from 'lodash'
import { Component } from 'react'
import { createStore } from 'redux'
import { Provider, connect } from 'react-redux'


const ADD_TRUFFEL = 'ADD_TRUFFEL'


function reducer (state = {}, action) {

    switch (action.type) {

        case ADD_TRUFFEL:
            return {
                ...state,
                truffels: state.truffels.concat([action.name]),
                totalTruffels: state.truffels.length + 1
            }

        default:
            return {
                ...state,
                totalTruffels: state.truffels.length
            }

    }

}

function addTruffel (name) {
    return {
        type: ADD_TRUFFEL,
        name: name
    }
}

const store = createStore(reducer, { truffels: [] })


@connect(state => ({state}))
class Truffels extends Component {

    render () {
        return (
            <div>
                <button onClick={() => store.dispatch(addTruffel('truffel'))}>add truffel</button>
                <TruffelCounter />
                {this.getTruffelEls()}
            </div>
        )
    }

    getTruffelEls () {
        return _.map(store.getState().truffels, (truffel, index) => {
            return <div key={index}>{truffel}</div>
        })
    }

}


class TruffelCounter extends Component {

    render () {
        return (
            <div>Total truffels: {store.getState().totalTruffels}</div>
        )
    }

}


ReactDOM.render(
    <Provider store={store}>
        <Truffels />
    </Provider>,
    document.getElementById('redux-2c')
)
