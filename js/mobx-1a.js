import React from 'react'
import ReactDOM from 'react-dom'
import _ from 'lodash'
import { Component } from 'react'
import { observable } from 'mobx'
import { observer } from 'mobx-react'


const truffels = observable([])


@observer
class Truffels extends Component {

    render () {
        return (
            <div>
                <button onClick={() => truffels.push('truffel')}>add truffel</button>
                {this.getTruffelEls()}
            </div>
        )
    }

    getTruffelEls () {
        return _.map(truffels, (truffel, index) => {
            return <div key={index}>{truffel}</div>
        })
    }

}


ReactDOM.render(
    <Truffels />,
    document.getElementById('mobx-1a')
)
