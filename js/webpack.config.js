module.exports = {
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',
                query: {
                    plugins: ['transform-decorators-legacy', 'transform-class-properties'],
                    presets: ['es2015', 'react', 'stage-2']
                }
            },
            {
                test: /\.json$/,
                loader: 'json'
            }
        ],
    },
    entry: {
        'app': './app.js',
    },
    output: {
        path: '../build/',
        filename: '[name].js'
    },
    node: {
        fs: 'empty',
        net: 'empty',
        tls: 'empty'
    }
};
