import React from 'react'
import ReactDOM from 'react-dom'
import _ from 'lodash'
import { Component } from 'react'
import { createStore } from 'redux'
import { Provider, connect } from 'react-redux'


const ADD_TRUFFEL = 'ADD_TRUFFEL'


function reducer (state = {}, action) {

    switch (action.type) {

        case ADD_TRUFFEL:
            return {
                ...state,
                truffels: state.truffels.concat([action.name]),
                totalTruffels: state.truffels.length + 1
            }

        default:
            return {
                ...state,
                totalTruffels: state.truffels.length
            }

    }

}

function addTruffel (name) {
    return {
        type: ADD_TRUFFEL,
        name: name
    }
}

const store = createStore(reducer, { truffels: [] })

function mapStateToProps (state) {
    return {
        truffels: state.truffels,
        totalTruffels: state.totalTruffels
    }
}

function mapDispatchToProps (dispatch) {
    return {
        onAddTruffelClick: name => {
            dispatch(addTruffel(name))
        }
    }
}


@connect(mapStateToProps, mapDispatchToProps)
class Truffels extends Component {

    render () {
        return (
            <div>
                <button onClick={() => this.props.onAddTruffelClick('truffel')}>add truffel</button>
                <TruffelCounter totalTruffels={this.props.totalTruffels} />
                {this.getTruffelEls()}
            </div>
        )
    }

    getTruffelEls () {
        return _.map(this.props.truffels, (truffel, index) => {
            return <div key={index}>{truffel}</div>
        })
    }

}


class TruffelCounter extends Component {

    render () {
        return (
            <div>Total truffels: {this.props.totalTruffels}</div>
        )
    }

}


ReactDOM.render(
    <Provider store={store}>
        <Truffels />
    </Provider>,
    document.getElementById('redux-2a')
)
