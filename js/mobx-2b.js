import React from 'react'
import ReactDOM from 'react-dom'
import _ from 'lodash'
import { Component } from 'react'
import { observable, action, computed } from 'mobx'
import { observer } from 'mobx-react'


class Store {

    @observable truffels = []

    @action addTruffel (name) {
        this.truffels.push(name)
    }

    @computed get totalTruffels () {
        return this.truffels.length
    }

}


const store = new Store()


@observer
class Truffels extends Component {

    render () {
        return (
            <div>
                <button onClick={() => store.addTruffel('truffels')}>add truffel</button>
                <TruffelCounter />
                {this.getTruffelEls()}
            </div>
        )
    }

    getTruffelEls () {
        return _.map(store.truffels, (truffel, index) => {
            return <div key={index}>{truffel}</div>
        })
    }

}


@observer
class TruffelCounter extends Component {

    render () {
        return (
            <div>Total truffels: {store.totalTruffels}</div>
        )
    }

}


ReactDOM.render(<Truffels />, document.getElementById('mobx-2b'))
