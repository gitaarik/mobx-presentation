import React from 'react'
import ReactDOM from 'react-dom'
import _ from 'lodash'
import { Component } from 'react'
import { observable, action } from 'mobx'
import { observer } from 'mobx-react'


class Store {

    @observable truffels = []

    @action addTruffel (name) {
        this.truffels.push(name)
    }

}


const store = new Store()


@observer
class Truffels extends Component {

    render () {
        return (
            <div>
                <button onClick={() => this.props.onAddTruffelClick('truffels')}>add truffel</button>
                {this.getTruffelEls()}
            </div>
        )
    }

    getTruffelEls () {
        return _.map(this.props.truffels, (truffel, index) => {
            return <div key={index}>{truffel}</div>
        })
    }

}


function getTruffelsElement () {
    return React.createElement(
        Truffels,
        {
            truffels: store.truffels,
            onAddTruffelClick: name => store.addTruffel(name)
        }
    )
}

ReactDOM.render(
    getTruffelsElement(),
    document.getElementById('mobx-1d')
)
