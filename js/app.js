import React from 'react'
import ReactDOM from 'react-dom'
import _ from 'lodash'
import { Component } from 'react'
import { observable } from 'mobx'
import { observer } from 'mobx-react'
import './redux-1a'
import './mobx-1a'
import './redux-1b'
import './mobx-1b'
import './mobx-1c'
import './mobx-1d'
import './redux-2a'
import './redux-2b'
import './redux-2c'
import './mobx-2a'
import './mobx-2b'
import './mobx-2c'
import './mobx-2d'


class Store {
    @observable page = ''
}


const store = new Store()


@observer
class MobxPresentation extends Component {

    componentDidMount () {
        this.hideAllPages()
    }

    render () {

        return (
            <div>
                <ul style={{display: 'flex', padding: 0, listStyle: 'none'}}>
                    {this.getPageEls()}
                </ul>
            </div>
        )

    }

    getPageEls () {

        const pages = [
            'redux-1a',
            'mobx-1a',
            'redux-1b',
            'mobx-1b',
            'mobx-1c',
            'mobx-1d',
            'redux-2a',
            'redux-2b',
            'redux-2c',
            'mobx-2a',
            'mobx-2b',
            'mobx-2c',
            'mobx-2d'
        ]

        return _.map(pages, (page, index) => {

            return (
                <li
                    key={index}
                    style={getStyle()}
                    onClick={this.showPage.bind(this, page)}
                >
                    {page}
                </li>
            )

            function getStyle () {

                return _.merge(
                    {
                        margin: 5,
                        padding: 5,
                        border: '1px solid #999',
                        cursor: 'pointer'
                    },
                    getActivePageStyle()
                )

                function getActivePageStyle () {
                    if (page == store.page) {
                        return { background: '#ADD8E6' }
                    }
                }

            }

        })

    }

    showPage (page) {
        store.page = page
        this.hideAllPages()
        document.getElementById(page).style.display = 'block'
    }

    hideAllPages () {
        _.each(
            document.getElementById('apps').children,
            child => child.style.display = 'none'
        )
    }

}


ReactDOM.render(
    <MobxPresentation />,
    document.getElementById('menu')
)
