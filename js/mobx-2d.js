import React from 'react'
import ReactDOM from 'react-dom'
import _ from 'lodash'
import { Component } from 'react'
import { observable, action, computed } from 'mobx'
import { Provider, observer, inject } from 'mobx-react'


class Store {

    @observable truffels = []

    @action addTruffel (name) {
        this.truffels.push(name)
    }

    @computed get totalTruffels () {
        return this.truffels.length
    }

}


const store = new Store()


@inject('store')
@observer
class Truffels extends Component {

    render () {
        return (
            <div>
                <button onClick={() => this.props.store.addTruffel('truffels')}>add truffel</button>
                <TruffelCounter />
                {this.getTruffelEls()}
            </div>
        )
    }

    getTruffelEls () {
        return _.map(this.props.store.truffels, (truffel, index) => {
            return <div key={index}>{truffel}</div>
        })
    }

}


@inject('store')
@observer
class TruffelCounter extends Component {

    render () {
        return (
            <div>Total truffels: {this.props.store.totalTruffels}</div>
        )
    }

}


ReactDOM.render(
    <Provider store={store}>
        <Truffels />
    </Provider>,
    document.getElementById('mobx-2d')
)
