import React from 'react'
import ReactDOM from 'react-dom'
import _ from 'lodash'
import { Component } from 'react'
import { observable, action } from 'mobx'
import { observer } from 'mobx-react'


class Store {

    @observable truffels = []

    @action addTruffel (name) {
        this.truffels.push(name)
    }

}


const store = new Store()


@observer
class Truffels extends Component {

    render () {
        return (
            <div>
                <button onClick={() => store.addTruffel('truffel')}>add truffel</button>
                {this.getTruffelEls()}
            </div>
        )
    }

    getTruffelEls () {
        return _.map(store.truffels, (truffel, index) => {
            return <div key={index}>{truffel}</div>
        })
    }

}


ReactDOM.render(
    <Truffels />,
    document.getElementById('mobx-1b')
)
